import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { CaseFormControls } from './modules/case-form/case-form.controls';
import { ContactFormControls } from './modules/contact-form/contact-form.controls';
import { LocationFormControls } from './modules/location-form/location-form.controls';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  formGroup!: FormGroup;
  locationForm!: FormGroup;
  contactForm!: FormGroup;
  contactLocationForm!: FormGroup;

  constructor(private readonly _fb: FormBuilder) {}

  ngOnInit() {
    this.contactLocationForm = this._fb.group(new LocationFormControls());
    this.contactForm = this._fb.group(new ContactFormControls());
    this.locationForm = this._fb.group(new LocationFormControls());
    const caseControls = new CaseFormControls();

    this.formGroup = this._fb.group({
      ...caseControls,
      location: this.locationForm,
    });
  }
}
