import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CaseFormModule } from './modules/case-form/case-form.module';
import { ContactFormModule } from './modules/contact-form/contact-form.module';
import { LocationFormModule } from './modules/location-form/location-form.module';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserAnimationsModule,
    CommonModule,
    ReactiveFormsModule,
    CaseFormModule,
    LocationFormModule,
    ContactFormModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
