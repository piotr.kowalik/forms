import { FormControl, Validators } from '@angular/forms';

export class ContactFormControls {
  name = new FormControl(null, Validators.required);
  phone = new FormControl(null, Validators.required);
  location = new FormControl(null, Validators.required);
}
