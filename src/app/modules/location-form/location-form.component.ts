import { Component, OnInit, Optional, Self } from '@angular/core';
import {
  ControlContainer,
  ControlValueAccessor,
  FormGroup,
  NgControl,
} from '@angular/forms';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-location-form',
  templateUrl: './location-form.component.html',
  styleUrls: ['./location-form.component.scss'],
})
export class LocationFormComponent implements OnInit, ControlValueAccessor {
  form!: FormGroup;
  private readonly _value$ = new BehaviorSubject<any>(null);
  private onChange?: (value: any) => void;
  private onTouched?: () => void;

  constructor(
    private readonly _container: ControlContainer,
    @Optional() @Self() private readonly _ngControl?: NgControl
  ) {
    if (this._ngControl) {
      this._ngControl.valueAccessor = this;
    }
  }

  ngOnInit(): void {
    this.form = this._container.control as FormGroup;
    this._value$.subscribe((value) => this.form.patchValue(value || {}));
    this.form.valueChanges.subscribe((value) => {
      if (this.onChange && this.onTouched) {
        this.form.valid ? this.onChange(value) : this.onChange(null);

        this.onTouched();
      }
    });
  }

  writeValue(obj: any): void {
    this._value$.next(obj);
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }
}
