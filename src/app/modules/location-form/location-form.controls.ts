import { FormControl, Validators } from '@angular/forms';

export class LocationFormControls {
  place = new FormControl(null);
  address = new FormControl(null, Validators.required);
  description = new FormControl(null);
}
