import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LocationFormComponent } from './location-form.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatButtonModule } from '@angular/material/button';
import { ReactiveFormsModule } from '@angular/forms';

const material = [
  MatFormFieldModule,
  MatInputModule,
  MatSelectModule,
  MatButtonModule,
];

@NgModule({
  declarations: [LocationFormComponent],
  imports: [CommonModule, ReactiveFormsModule, ...material],
  exports: [LocationFormComponent],
})
export class LocationFormModule {}
