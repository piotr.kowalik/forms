import { FormControl, Validators } from '@angular/forms';

export class CaseFormControls {
  title = new FormControl(null, Validators.required);
  type = new FormControl(null, Validators.required);
  description = new FormControl();
  contact = new FormControl(null, Validators.required);

  disable() {
    this.title.disable();
    this.type.disable();
  }
}
