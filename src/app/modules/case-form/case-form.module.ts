import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';

import { CaseFormComponent } from './case-form.component';
import { ReactiveFormsModule } from '@angular/forms';

const material = [
  MatFormFieldModule,
  MatInputModule,
  MatSelectModule,
  MatButtonModule,
];

@NgModule({
  declarations: [CaseFormComponent],
  imports: [CommonModule, ReactiveFormsModule, ...material],
  exports: [CaseFormComponent],
})
export class CaseFormModule {}
