import { ControlContainer, FormGroup } from '@angular/forms';
import {
  ContentChild,
  EventEmitter,
  TemplateRef,
  Component,
  Directive,
  Output,
  OnInit,
  Input,
} from '@angular/core';

@Directive({
  selector: 'appTypeOption',
})
export class TypeOptionDirective {}

@Directive({
  selector: 'appLocationOption',
})
export class LocationOptionDirective {}

@Component({
  selector: 'app-case-form',
  templateUrl: './case-form.component.html',
  styleUrls: ['./case-form.component.scss'],
})
export class CaseFormComponent implements OnInit {
  form!: FormGroup;

  constructor(private readonly _container: ControlContainer) {}

  ngOnInit(): void {
    this.form = this._container.control as FormGroup;
  }
}
